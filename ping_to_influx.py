#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import re
from time import sleep
import argparse

from influxdb import InfluxDBClient


def parse_ping(ping_output):
    """
    Parses the output of a unix `ping -c1 <server>` command.
    Only the second line is of interest and thus analysed.

    :param ping_output: the bytes string output by a call to `ping`
    :return: a dictionary with target reached, ttl and latency.
    """
    lines = ping_output.decode().splitlines()
    ping_expr = r'(.*)from (.*): (.*)ttl=(\d+) time=(\d+\.?\d*)'
    m = re.match(ping_expr, lines[1])
    target = m.group(2)
    ttl = m.group(4)
    latency = m.group(5)
    return {'latency': float(latency),
            'ttl': int(ttl),
            'target_from': target}


def log_to_influx(db, data, name):
    """
    Take data a dictionary and write its content to `db`, to the `name` series,
    using the keys as columns and the values as points.

    :param db:
    :param data:
    :param name:
    """
    json_data = [{
        "points": [
            list(data.values())
        ],
        "name": name,
        "columns": list(data.keys())
    }]
    db.write_points(json_data)


def track_ping(db, target):
    """
    call for one ping to `target` server and log results to influxdb `db`.
    :param db: the InfluxDBClient instance to write to
    :param target: the server to ping
    """
    data = {'target_to': target,
            'up': 1,
            'target_from': 'unknown',
            }
    try:
        ping_output = subprocess.check_output(['ping', '-c1', target])
        data.update(parse_ping(ping_output))
        log_to_influx(db, data, name="ping_monitoring")
    except subprocess.CalledProcessError:
        data['up'] = 0
        log_to_influx(db, data, name="ping_monitoring")


def main():
    """
    Pings and fill an InfluxDB database with the results.

    ```
    usage: ping_to_influx.py [-h] [-i I] destination
    ```

    It will ping `destination` every `I` seconds. Multiple addresses can be given in `destination`,
    separated with a comma.

    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", type=int,
                        help="Wait `i` seconds between sending each ping command series.",
                        default=60)
    parser.add_argument("destination", type=str,
                        help="Destination of the ping, multiple addresses can be given separated by a comma.")
    args = parser.parse_args()

    targets = args.destination.split(',')
    ping_db = InfluxDBClient(database='pingtest')
    while(True):
        for target in targets:
            track_ping(ping_db, target.strip())
        sleep(args.i)



if __name__ == '__main__':
    main()